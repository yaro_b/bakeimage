//
//  main.cpp
//  mio::bakeimage
//
//  Copyright (c) 2013 mio. All rights reserved.
//

//#define STBI_HEADER_FILE_ONLY
#define STBI_FAILURE_USERMSG
#include "../3rd_party/stb/stb_image.c"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../3rd_party/stb/stb_image_write.h"

#include "../3rd_party/lua/src/lua.hpp"
#include "../3rd_party/lua/src/lauxlib.h"
#include "../3rd_party/lua/src/lualib.h"

#include <cstdio>
#include <cstdlib>


////////////////////////////////////////////////////////////////////////////////////////////////////

struct Config
{
    const char* input;
    const char* output;
    const char* script;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

void print_help();
void parse_program_options(Config& config, int argc, char** argv);

void error(lua_State* L, const char* format, ...);
void stack_dump(lua_State* L);

void call_get_size(lua_State* L, int& width, int& height);
void call_get_pixel(lua_State* L, unsigned char* rgba, int row, int column);

void generic_lua_call(lua_State* L, const char* function, const char* signature, ...);

////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    Config config;

    #if 1
        parse_program_options(config, argc, argv);
    #else
        // Used for debug.
        config.input = "../data/input.png";
        config.output = "../data/output.png";
        config.script = "../data/sample.lua";
    #endif

    // bpp - bytes per pixel.
    int w, h, bpp;
    unsigned char* data = 0;

    // Init Lua.
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);

    // Load script.
    if (luaL_loadfile(L, config.script) || lua_pcall(L, 0, 0, 0))
    {
        error(L, "Lua error: %s.\n", lua_tostring(L, -1));
    }

    // Init image data.
    const bool has_source_image = (config.input != 0);
    if (has_source_image)
    {
        // Load LDR image.
        data = stbi_load(config.input, &w, &h, &bpp, 0);
        if (!data)
        {
            printf("ERROR: %s.\n", stbi_failure_reason());
            return EXIT_FAILURE;
        }
    }
    else
    {
        #if 0
            call_get_size(L, w, h);
        #else
            generic_lua_call(L, "get_size", ">ii", &w, &h);
        #endif

        bpp = 4;

        // Alloc new image data.
        data = new unsigned char[w * h * bpp];
    }

    for (int row = 0; row < h; ++row)
    {
        for (int col = 0; col < w; ++col)
        {
            #if 0
                call_get_pixel(L, data + (row * w + col) * bpp, row, col);
            #else
                int r, g, b, a;
                generic_lua_call(L, "get_pixel", "ii>iiii", row, col, &r, &g, &b, &a);

                unsigned char* rgba = data + (row * w + col) * bpp;
                *rgba++ = 0xff & r;
                *rgba++ = 0xff & g;
                *rgba++ = 0xff & b;
                *rgba++ = 0xff & a;
            #endif
        }
    }

    // Write image.
    const int success = stbi_write_png(config.output, w, h, bpp, data, 0);
    if (!success)
    {
        printf("ERROR: Failed to write PNG image.");
        return EXIT_FAILURE;
    }

    if (has_source_image)
        stbi_image_free(data);
    else
        delete [] data;

    lua_close(L);

    return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void print_help()
{
    printf(
        "bakeimage (c) mio, 2013\n"
        "Usage:\n"
        "\tbakeimage [-i input.png] -o output.png -l script.lua\n"
        "\tbakeimage -h\n");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void parse_program_options(Config& config, int argc, char** argv)
{
    config.input = 0;
    config.output = 0;
    config.script = 0;

    bool show_help_message = (argc == 1);

    for (int i = 1; i < argc; ++i)
    {
        if (strcmp("-i", argv[i]) == 0 && i + 1 < argc)
        {
            config.input = argv[++i];
        }
        else if (strcmp("-o", argv[i]) == 0 && i + 1 < argc)
        {
            config.output = argv[++i];
        }
        else if (strcmp("-l", argv[i]) == 0 && i + 1 < argc)
        {
            config.script = argv[++i];
        }
        else if (strcmp("-h", argv[i]) == 0)
        {
            show_help_message = true;
        }
        else
        {
            printf("ERROR: Unknown option: %s\n", argv[i]);
            print_help();
            exit(EXIT_FAILURE);
        }
    }

    if (show_help_message)
    {
        print_help();
        exit(EXIT_SUCCESS);
    }

    if (config.output == 0)
    {
        printf("ERROR: Output file is not specified.\n");
        exit(EXIT_FAILURE);
    }
    if (config.script == 0)
    {
        printf("ERROR: Lua script file is not specified.\n");
        exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void error(lua_State* L, const char* format, ...)
{
    va_list(argp);
    va_start(argp, format);
    vfprintf(stdout, format, argp);
    va_end(argp);

    lua_close(L);
    exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void stack_dump(lua_State* L)
{
    printf("Lua stack dump:\n");

    for (int i = 1, n = lua_gettop(L); i <= n; ++i)
    {
        const int type = lua_type(L, i);
        switch (type)
        {
            case LUA_TSTRING:
                printf("[%02i] '%s'\n", i, lua_tostring(L, i));
                break;

            case LUA_TBOOLEAN:
                printf("[%02i] %s\n", i, lua_toboolean(L, i) ? "true" : "false");
                break;

            case LUA_TNUMBER:
                printf("[%02i] %g\n", i, lua_tonumber(L, i));
                break;

            default:
                printf("[%02i] %s\n", i, lua_typename(L, type));
                break;
        }
    }

    printf("<end>\n\n");
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void call_get_size(lua_State* L, int& width, int& height)
{
    lua_getglobal(L, "get_size");
    if (lua_pcall(L, 0, 2, 0) != 0)
    {
        error(L, "Lua error: get_size() failed - %s.\n", lua_tostring(L, -1));
    }

    if (!lua_isnumber(L, -2) ||
        !lua_isnumber(L, -1))
    {
        error(L, "Lua error: get_size() should return 2 numbers - image width and height.\n");
    }

    width = lua_tointeger(L, -2);
    height = lua_tointeger(L, -1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void call_get_pixel(lua_State* L, unsigned char* rgba, int row, int column)
{
    lua_getglobal(L, "get_pixel");
    lua_pushnumber(L, row);
    lua_pushnumber(L, column);

    if (lua_pcall(L, 2, 4, 0) != 0)
    {
        error(L, "Lua error: get_pixel() failed - %s.\n", lua_tostring(L, -1));
    }

    if (!lua_isnumber(L, -4) ||
        !lua_isnumber(L, -3) ||
        !lua_isnumber(L, -2) ||
        !lua_isnumber(L, -1))
    {
        error(L, "Lua error: get_pixel() should return 4 numbers - RGBA values.\n");
    }

    *rgba++ = 0xff & lua_tointeger(L, -4);
    *rgba++ = 0xff & lua_tointeger(L, -3);
    *rgba++ = 0xff & lua_tointeger(L, -2);
    *rgba++ = 0xff & lua_tointeger(L, -1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void generic_lua_call(lua_State* L, const char* function, const char* signature, ...)
{
    va_list vl;
    va_start(vl, signature);

    lua_getglobal(L, function);

    // Push arguments.
    int argument_count;
    for (argument_count = 0; *signature && *signature != '>'; ++argument_count)
    {
        // Check Lua stack space.
        luaL_checkstack(L, 1, "too many arguments");

        switch (*signature++)
        {
            case 'd':
                lua_pushnumber(L, va_arg(vl, double));
                break;

            case 'i':
                lua_pushinteger(L, va_arg(vl, int));
                break;

            case 's':
                lua_pushstring(L, va_arg(vl, char*));
                break;

            default:
                error(L, "ERROR: Invalid option (%c).\n", *(signature - 1));
        }
    }
    // Skip the '>' character.
    if (*signature) ++signature;

    const int result_count = strlen(signature);

    // Call.
    if (lua_pcall(L, argument_count, result_count, 0) != 0)
    {
        error(L, "Lua error: %s() call failed - %s.\n", function, lua_tostring(L, -1));
    }

    // Pop results.
    for (int i = -result_count; *signature; ++i)
    {
        switch (*signature++)
        {
            case 'd':
                if (!lua_isnumber(L, i))
                    error(L, "Lua error: Wrong result type - number expected.\n");
                *va_arg(vl, double*) = lua_tonumber(L, i);
                break;

            case 'i':
                if (!lua_isnumber(L, i))
                    error(L, "Lua error: Wrong result type - number expected.\n");
                *va_arg(vl, int*) = lua_tointeger(L, i);

                break;

            case 's':
                if (!lua_isstring(L, i))
                    error(L, "Lua error: Wrong result type - string expected.\n");
                *va_arg(vl, const char**) = lua_tostring(L, i);
                break;

            default:
                error(L, "ERROR: Invalid option (%c).\n", *(signature - 1));
        }
    }

    va_end(vl);
}
