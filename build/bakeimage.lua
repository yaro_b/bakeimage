--
-- Premake config file for 'bakeimage' tool.
--

-- Reset RNG seed to get consistent results across runs (i.e. XCode).
math.randomseed(1337)

solution "bakeimage"
    configurations {"Debug", "Release"}
    debugdir ("../bin")
    targetdir "../bin"
    flags {"NoPCH"}
    location (_ACTION)

    configuration "Debug"
        defines {"_DEBUG"}
        flags {"Symbols", "ExtraWarnings"}
        targetsuffix "-d"

    configuration "Release"
        defines {"NDEBUG"}
        flags {"Optimize", "ExtraWarnings"}

    configuration {}

    project "bakeimage"
        uuid "7199b610-41b2-11e3-aa6e-0800200c9a66"
        kind "ConsoleApp"
        language "C++"
        files {"../src/**"}
        links {"stb", "lua"}

    project "stb"
        uuid "7199dd20-41b2-11e3-aa6e-0800200c9a66"
        kind "StaticLib"
        language "C"
        files {"../3rd_party/stb/**"}

    project "lua"
        uuid "7199dd21-41b2-11e3-aa6e-0800200c9a66"
        kind "StaticLib"
        language "C"
        files {"../3rd_party/lua/**"}
