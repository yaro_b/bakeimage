function get_size()
    -- width, height
    return 256, 256
end

function get_pixel(row, column)
    -- R, G, B, A
    return column, row, 0, 255
end
